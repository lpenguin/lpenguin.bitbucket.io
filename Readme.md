# AR Genes site

## Dependicies

* Ruby

```bash
sudo apt-get install rubygems
```

* [Jekkyl](https://jekyllrb.com/docs)

```bash
sudo -H gem install jekyll bundler
```

## Building

* `git clone` repository
* `cd` into it
* Build project

```
bundle exec jekyll build
```

* Output will be in `_site` folder

## Developing

* Edit `.html` or `.md` files

* Add or remove navgation links via `_config.yml`: `navigation` variable

```yaml
navigation:
  - title: ResistoMap  # Page title
    url: /resistomap-description.html # Page url
  - title: Наши публикации
    url: /articles.html
```

* Run development server

```bash
bundle exec jekyll serve -H 0.0.0.0 -P 4000
```